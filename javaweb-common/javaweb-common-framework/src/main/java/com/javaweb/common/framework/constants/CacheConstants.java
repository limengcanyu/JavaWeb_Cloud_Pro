// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.common.framework.constants;

/**
 * 缓存常量
 */
public class CacheConstants {

    /**
     * OAuth缓存前缀
     */
    public static final String OAUTH_ACCESS = "oauth:access:";

    /**
     * OAuth客户端信息
     */
    public static final String CLIENT_DETAILS_KEY = "oauth:client:details";

}
