// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package ${packageName}.controller;

import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import ${packageName}.entity.${entityName};
import ${packageName}.query.${entityName}Query;
import ${packageName}.service.I${entityName}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * ${tableAnnotation} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@RestController
@RequestMapping("/${entityName?lower_case}")
public class ${entityName}Controller extends BaseController {

    @Autowired
    private I${entityName}Service ${entityName?uncap_first}Service;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @PostMapping("/list")
    public JsonResult list(@RequestBody ${entityName}Query query) {
        return ${entityName?uncap_first}Service.getList(query);
    }

    /**
     * 添加记录
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    public JsonResult add(@RequestBody ${entityName} entity) {
        return ${entityName?uncap_first}Service.edit(entity);
    }

    /**
     * 获取详情
     *
     * @param ${entityName?lower_case}Id 记录ID
     * @return
     */
    @GetMapping("/info/{${entityName?lower_case}Id}")
    public JsonResult info(@PathVariable("${entityName?lower_case}Id") Integer ${entityName?lower_case}Id) {
        return ${entityName?uncap_first}Service.info(${entityName?lower_case}Id);
    }

    /**
     * 更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody ${entityName} entity) {
        return ${entityName?uncap_first}Service.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param ${entityName?lower_case}Ids 记录ID
     * @return
     */
    @DeleteMapping("/delete/{${entityName?lower_case}Ids}")
    public JsonResult delete(@PathVariable("${entityName?lower_case}Ids") Integer[] ${entityName?lower_case}Ids) {
        return ${entityName?uncap_first}Service.deleteByIds(${entityName?lower_case}Ids);
    }
}