// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.member.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.member.entity.MemberLevel;
import com.javaweb.service.member.query.MemberLevelQuery;
import com.javaweb.service.member.service.IMemberLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 会员级别表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-04
 */
@RestController
@RequestMapping("/memberlevel")
public class MemberLevelController extends BaseController {

    @Autowired
    private IMemberLevelService memberLevelService;

    /**
     * 获取会员等级列表
     *
     * @param memberLevelQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    public JsonResult index(MemberLevelQuery memberLevelQuery) {
        return memberLevelService.getList(memberLevelQuery);
    }

    /**
     * 添加会员等级
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    public JsonResult add(@RequestBody MemberLevel entity) {
        return memberLevelService.edit(entity);
    }

    /**
     * 编辑会员等级
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody MemberLevel entity) {
        return memberLevelService.edit(entity);
    }

    /**
     * 删除会员等级
     *
     * @param memberLevelIds 会员ID
     * @return
     */
    @DeleteMapping("/delete/{memberLevelIds}")
    public JsonResult delete(@PathVariable("memberLevelIds") Integer[] memberLevelIds) {
        return memberLevelService.deleteByIds(memberLevelIds);
    }

    /**
     * 获取会员等级列表
     *
     * @return
     */
    @GetMapping("/getMemberLevelList")
    public JsonResult getMemberLevelList() {
        return memberLevelService.getMemberLevelList();
    }

}
