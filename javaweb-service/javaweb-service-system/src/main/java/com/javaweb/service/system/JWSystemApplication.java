// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system;

import com.javaweb.common.security.annotation.EnableCustomConfig;
import com.javaweb.common.security.annotation.EnableJWFeignClients;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@EnableJWFeignClients
@EnableCustomConfig
@SpringCloudApplication
@EnableResourceServer
public class JWSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(JWSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  JavaWeb_Cloud_Pro微服务旗舰版【系统服务】启动成功   ლ(´ڡ`ლ)ﾞ");
    }

}
