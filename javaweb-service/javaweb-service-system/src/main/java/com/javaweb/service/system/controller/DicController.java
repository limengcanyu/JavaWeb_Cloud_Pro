// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.Dic;
import com.javaweb.service.system.query.DicQuery;
import com.javaweb.service.system.service.IDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字典项管理表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-01
 */
@RestController
@RequestMapping("/dic")
public class DicController extends BaseController {

    @Autowired
    private IDicService dicService;

    /**
     * 获取字典项列表
     *
     * @param dicQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    public JsonResult index(DicQuery dicQuery) {
        return dicService.getList(dicQuery);
    }

    /**
     * 添加字典项
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    public JsonResult add(@RequestBody Dic entity) {
        return dicService.edit(entity);
    }

    /**
     * 编辑字典项
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Dic entity) {
        return dicService.edit(entity);
    }

    /**
     * 删除字典项
     *
     * @param dicIds 字典项ID
     * @return
     */
    @DeleteMapping("/delete/{dicIds}")
    public JsonResult delete(@PathVariable("dicIds") Integer[] dicIds) {
        return dicService.deleteByIds(dicIds);
    }

}
