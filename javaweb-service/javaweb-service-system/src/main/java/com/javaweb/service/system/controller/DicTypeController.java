// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.entity.DicType;
import com.javaweb.service.system.query.DicTypeQuery;
import com.javaweb.service.system.service.IDicTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 字典类型表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-01
 */
@RestController
@RequestMapping("/dictype")
public class DicTypeController extends BaseController {

    @Autowired
    private IDicTypeService dicTypeService;

    /**
     * 获取字典列表
     *
     * @param dicTypeQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    public JsonResult index(DicTypeQuery dicTypeQuery) {
        return dicTypeService.getList(dicTypeQuery);
    }

    /**
     * 添加字典
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    public JsonResult add(@RequestBody DicType entity) {
        return dicTypeService.edit(entity);
    }

    /**
     * 编辑字典
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody DicType entity) {
        return dicTypeService.edit(entity);
    }

    /**
     * 删除字典
     *
     * @param dicTypeId 字典ID
     * @return
     */
    @DeleteMapping("/delete/{dicTypeId}")
    public JsonResult delete(@PathVariable("dicTypeId") Integer dicTypeId) {
        return dicTypeService.deleteById(dicTypeId);
    }

}
