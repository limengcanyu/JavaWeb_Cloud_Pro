// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.controller;


import com.javaweb.api.system.entity.User;
import com.javaweb.api.system.entity.UserInfo;
import com.javaweb.common.framework.common.BaseController;
import com.javaweb.common.framework.config.CommonConfig;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.query.UserQuery;
import com.javaweb.service.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 后台用户管理表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-10-30
 */
@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    @Autowired
    private IUserService userService;

    /**
     * 获取用户列表
     *
     * @param userQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    public JsonResult index(UserQuery userQuery) {
        return userService.getList(userQuery);
    }

    /**
     * 添加用户
     *
     * @param entity 实体对象
     * @return
     */
    @PostMapping("/add")
    public JsonResult add(@RequestBody User entity) {
        return userService.edit(entity);
    }

    /**
     * 编辑用户
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody User entity) {
        return userService.edit(entity);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @PutMapping("/status")
    public JsonResult status(@RequestBody User entity) {
        return userService.setStatus(entity);
    }

    /**
     * 删除用户
     *
     * @param userIds 用户ID
     * @return
     */
    @DeleteMapping("/delete/{userIds}")
    public JsonResult delete(@PathVariable("userIds") Integer[] userIds) {
        return userService.deleteByIds(userIds);
    }

    /**
     * 获取用户信息
     *
     * @param username 用户名
     * @return
     */
    @GetMapping("/info/{username}")
    public JsonResult<UserInfo> info(@PathVariable("username") String username) {
        return userService.getInfoByName(username);
    }


}
