// +----------------------------------------------------------------------
// | JavaWeb_Cloud_Pro微服务旗舰版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.service.system.service;

import com.javaweb.api.system.entity.User;
import com.javaweb.api.system.entity.UserInfo;
import com.javaweb.common.framework.common.IBaseService;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.service.system.dto.UpdatePwdDto;
import com.javaweb.service.system.dto.UpdateUserInfoDto;
import com.javaweb.service.system.query.UserQuery;

/**
 * <p>
 * 后台用户管理表 服务类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-10-30
 */
public interface IUserService extends IBaseService<User> {

    /**
     * 根据用户名获取用户信息
     *
     * @param username 用户名
     * @return
     */
    JsonResult<UserInfo> getInfoByName(String username);

    /**
     * 根据查询条件获取数据列表
     *
     * @param userQuery 查询条件
     * @return
     */
    JsonResult getList(UserQuery userQuery);

    /**
     * 根据实体对象添加、编辑记录
     *
     * @param entity 实体对象
     * @return
     */
    JsonResult edit(User entity);

    /**
     * 根据ID删除记录
     *
     * @param id 记录ID
     * @return
     */
    JsonResult deleteById(Integer id);

    /**
     * 获取用户信息
     *
     * @return
     */
    JsonResult getUserInfo();

    /**
     * 修改密码
     *
     * @param updatePwdDto 参数
     * @return
     */
    JsonResult updatePwd(UpdatePwdDto updatePwdDto);

    /**
     * 更新个人资料
     *
     * @param updateUserInfoDto 参数
     * @return
     */
    JsonResult updateUserInfo(UpdateUserInfoDto updateUserInfoDto);

}
